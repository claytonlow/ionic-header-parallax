/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { ParallaxDirective } from './parallax.directive';
var IonicHeaderParallaxModule = /** @class */ (function () {
    function IonicHeaderParallaxModule() {
    }
    IonicHeaderParallaxModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        ParallaxDirective
                    ],
                    imports: [],
                    exports: [
                        ParallaxDirective
                    ]
                },] }
    ];
    return IonicHeaderParallaxModule;
}());
export { IonicHeaderParallaxModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtaGVhZGVyLXBhcmFsbGF4Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2lvbmljLWhlYWRlci1wYXJhbGxheC8iLCJzb3VyY2VzIjpbImxpYi9pb25pYy1oZWFkZXItcGFyYWxsYXgubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXpEO0lBQUE7SUFVeUMsQ0FBQzs7Z0JBVnpDLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUU7d0JBQ1osaUJBQWlCO3FCQUNsQjtvQkFDRCxPQUFPLEVBQUUsRUFDUjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsaUJBQWlCO3FCQUNsQjtpQkFDRjs7SUFDd0MsZ0NBQUM7Q0FBQSxBQVYxQyxJQVUwQztTQUE3Qix5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQYXJhbGxheERpcmVjdGl2ZSB9IGZyb20gJy4vcGFyYWxsYXguZGlyZWN0aXZlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBQYXJhbGxheERpcmVjdGl2ZVxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgUGFyYWxsYXhEaXJlY3RpdmVcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBJb25pY0hlYWRlclBhcmFsbGF4TW9kdWxlIHsgfVxyXG4iXX0=