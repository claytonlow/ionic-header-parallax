import { ElementRef, Renderer2, AfterViewInit , OnChanges} from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class ParallaxDirective implements AfterViewInit, OnChanges {
    headerRef: ElementRef<HTMLElement>;
    renderer: Renderer2;
    imageUrl: string;
    expandedColor: string;
    titleColor: string;
    maximumHeight: number;
    header: HTMLElement;
    toolbar: HTMLElement;
    toolbarBackground: HTMLElement;
    imageOverlay: HTMLElement;
    colorOverlay: HTMLElement;
    barButtons: HTMLElement;
    scrollContent: HTMLElement;
    headerHeight: any;
    headerMinHeight: number;
    translateAmt: any;
    scaleAmt: any;
    scrollTop: any;
    lastScrollTop: any;
    ticking: any;
    originalToolbarBgColor: string;
    overlayTitle: HTMLElement;
    ionTitle: HTMLElement;
    overlayButtons: HTMLElement[];
    scrollContentPaddingTop: any;
    constructor(headerRef: ElementRef<HTMLElement>, renderer: Renderer2);
    ngAfterViewInit(): void;
    ngOnChanges(): void;
    initElements(): void;
    initStyles(): void;
    initEvents(): void;
    updateElasticHeader(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ParallaxDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<ParallaxDirective, "ion-header[parallax]", never, { "maximumHeight": "maximumHeight"; "imageUrl": "imageUrl"; "expandedColor": "expandedColor"; "titleColor": "titleColor"; }, {}, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFyYWxsYXguZGlyZWN0aXZlLmQudHMiLCJzb3VyY2VzIjpbInBhcmFsbGF4LmRpcmVjdGl2ZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0NBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRWxlbWVudFJlZiwgUmVuZGVyZXIyLCBBZnRlclZpZXdJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIFBhcmFsbGF4RGlyZWN0aXZlIGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgICBoZWFkZXJSZWY6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+O1xyXG4gICAgcmVuZGVyZXI6IFJlbmRlcmVyMjtcclxuICAgIGltYWdlVXJsOiBzdHJpbmc7XHJcbiAgICBleHBhbmRlZENvbG9yOiBzdHJpbmc7XHJcbiAgICB0aXRsZUNvbG9yOiBzdHJpbmc7XHJcbiAgICBtYXhpbXVtSGVpZ2h0OiBudW1iZXI7XHJcbiAgICBoZWFkZXI6IEhUTUxFbGVtZW50O1xyXG4gICAgdG9vbGJhcjogSFRNTEVsZW1lbnQ7XHJcbiAgICB0b29sYmFyQmFja2dyb3VuZDogSFRNTEVsZW1lbnQ7XHJcbiAgICBpbWFnZU92ZXJsYXk6IEhUTUxFbGVtZW50O1xyXG4gICAgY29sb3JPdmVybGF5OiBIVE1MRWxlbWVudDtcclxuICAgIGJhckJ1dHRvbnM6IEhUTUxFbGVtZW50O1xyXG4gICAgc2Nyb2xsQ29udGVudDogSFRNTEVsZW1lbnQ7XHJcbiAgICBoZWFkZXJIZWlnaHQ6IGFueTtcclxuICAgIGhlYWRlck1pbkhlaWdodDogbnVtYmVyO1xyXG4gICAgdHJhbnNsYXRlQW10OiBhbnk7XHJcbiAgICBzY2FsZUFtdDogYW55O1xyXG4gICAgc2Nyb2xsVG9wOiBhbnk7XHJcbiAgICBsYXN0U2Nyb2xsVG9wOiBhbnk7XHJcbiAgICB0aWNraW5nOiBhbnk7XHJcbiAgICBvcmlnaW5hbFRvb2xiYXJCZ0NvbG9yOiBzdHJpbmc7XHJcbiAgICBvdmVybGF5VGl0bGU6IEhUTUxFbGVtZW50O1xyXG4gICAgaW9uVGl0bGU6IEhUTUxFbGVtZW50O1xyXG4gICAgb3ZlcmxheUJ1dHRvbnM6IEhUTUxFbGVtZW50W107XHJcbiAgICBzY3JvbGxDb250ZW50UGFkZGluZ1RvcDogYW55O1xyXG4gICAgY29uc3RydWN0b3IoaGVhZGVyUmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PiwgcmVuZGVyZXI6IFJlbmRlcmVyMik7XHJcbiAgICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZDtcclxuICAgIGluaXRFbGVtZW50cygpOiB2b2lkO1xyXG4gICAgaW5pdFN0eWxlcygpOiB2b2lkO1xyXG4gICAgaW5pdEV2ZW50cygpOiB2b2lkO1xyXG4gICAgdXBkYXRlRWxhc3RpY0hlYWRlcigpOiB2b2lkO1xyXG59XHJcbiJdfQ==