import { Directive, ElementRef, Renderer2, Input, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as ɵngcc0 from '@angular/core';
class ParallaxDirective {
    /**
     * @param {?} headerRef
     * @param {?} renderer
     */
    constructor(headerRef, renderer) {
        this.headerRef = headerRef;
        this.renderer = renderer;
        this.maximumHeight = 300;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.initElements();
            this.initStyles();
            this.initEvents();
        }), 0);
    }
    
    /**
     * @return {?}
     */
    initElements() {
        /** @type {?} */
        const parentElement = this.headerRef.nativeElement.parentElement;
        this.header = this.headerRef.nativeElement;
        this.toolbar = this.header.querySelector('ion-toolbar');
        if (!this.toolbar) {
            throw new Error('Parallax directive requires a toolbar or navbar element on the page to work.');
        }
        this.ionTitle = this.toolbar.querySelector('ion-title');
        this.toolbarBackground = this.toolbar.shadowRoot.querySelector('.toolbar-background');
        this.barButtons = this.headerRef.nativeElement.querySelector('ion-buttons');
        this.shareButtons = this.headerRef.nativeElement.querySelector('.share-buttons');
        
        /** @type {?} */
        const ionContent = parentElement.querySelector('ion-content');
        this.scrollContent = ionContent.shadowRoot.querySelector('.inner-scroll');
        if (!this.scrollContent) {
            throw new Error('Parallax directive requires an <ion-content> element on the page to work.');
        }
        // Create image overlay
        this.imageOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.imageOverlay, 'image-overlay');
        this.colorOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.colorOverlay, 'color-overlay');
        this.colorOverlay.appendChild(this.imageOverlay);
        this.header.appendChild(this.colorOverlay);
        // Copy title and buttons
        this.overlayTitle = this.ionTitle && (/** @type {?} */ (this.ionTitle.cloneNode(true)));
        if (this.overlayTitle) {
            this.renderer.addClass(this.overlayTitle, 'parallax-title');
            setTimeout((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const toolbarTitle = this.overlayTitle.shadowRoot.querySelector('.toolbar-title');
                if (toolbarTitle !== null) {
                    this.renderer.setStyle(toolbarTitle, 'pointer-events', 'unset');
                }
            }));
        }
        if (this.overlayTitle) {
            this.imageOverlay.appendChild(this.overlayTitle);
        }
        if (this.barButtons) {
            this.imageOverlay.appendChild(this.barButtons);
        }

        if (this.shareButtons) {
            this.imageOverlay.appendChild(this.shareButtons);
        }
    }
    /**
     * @return {?}
     */
    initStyles() {
        this.headerHeight = this.scrollContent.clientHeight;
        this.ticking = false;
        if (!this.scrollContent || !toolbar) {
            return;
        }
        // fetch styles
        this.maximumHeight = parseFloat(this.maximumHeight.toString());
        this.headerMinHeight = this.toolbar.offsetHeight;
        this.scrollContentPaddingTop = window.getComputedStyle(this.scrollContent, null).paddingTop.replace('px', '');
        this.scrollContentPaddingTop = parseFloat(this.scrollContentPaddingTop);
        this.originalToolbarBgColor = window.getComputedStyle(this.toolbarBackground, null).backgroundColor;
        // header and title
        this.renderer.setStyle(this.header, 'position', 'relative');
        if (this.overlayTitle) {
            this.renderer.setStyle(this.overlayTitle, 'color', this.titleColor);
            this.renderer.setStyle(this.overlayTitle, 'position', 'absolute');
            this.renderer.setStyle(this.overlayTitle, 'width', '100%');
            this.renderer.setStyle(this.overlayTitle, 'height', '100%');
            this.renderer.setStyle(this.overlayTitle, 'text-align', 'center');
        }
        // color overlay
        this.renderer.setStyle(this.colorOverlay, 'background-color', this.originalToolbarBgColor);
        this.renderer.setStyle(this.colorOverlay, 'height', `${this.maximumHeight}px`);
        this.renderer.setStyle(this.colorOverlay, 'position', 'absolute');
        this.renderer.setStyle(this.colorOverlay, 'top', `${-this.headerMinHeight * 0}px`);
        this.renderer.setStyle(this.colorOverlay, 'left', '0');
        this.renderer.setStyle(this.colorOverlay, 'width', '100%');
        this.renderer.setStyle(this.colorOverlay, 'z-index', '10');
        this.renderer.setStyle(this.colorOverlay, 'pointer-events', 'none');
        // image overlay
        this.renderer.setStyle(this.imageOverlay, 'background-color', this.expandedColor);
        this.renderer.setStyle(this.imageOverlay, 'background-image', `url(${this.imageUrl || ''})`);
        this.renderer.setStyle(this.imageOverlay, 'height', `100%`);
        this.renderer.setStyle(this.imageOverlay, 'width', '100%');
        this.renderer.setStyle(this.imageOverlay, 'pointer-events', 'none');
        this.renderer.setStyle(this.imageOverlay, 'background-size', 'cover');
        this.renderer.setStyle(this.imageOverlay, 'background-position', 'center');
        // .toolbar-background
        this.renderer.setStyle(this.toolbarBackground, 'background-color', this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            this.renderer.setStyle(this.barButtons, 'pointer-events', 'all');
            Array.from(this.barButtons.children).forEach((/**
             * @param {?} btn
             * @return {?}
             */
            btn => {
                this.renderer.setStyle(btn, 'color', this.titleColor);
            }));
        }

        if (this.shareButtons) {
            this.renderer.setStyle(this.shareButtons, 'pointer-events', 'all');
            Array.from(this.shareButtons.children).forEach((/**
             * @param {?} btn
             * @return {?}
             */
            btn => {
                this.renderer.setStyle(btn, 'color', this.titleColor);
            }));
        }
        // .scroll-content
        if (this.scrollContent) {
            this.renderer.setAttribute(this.scrollContent, 'parallax', '');
            this.renderer.setStyle(this.scrollContent, 'padding-top', `${this.maximumHeight + this.scrollContentPaddingTop - this.headerMinHeight}px`);
        }
    }
    /**
     * @return {?}
     */
    initEvents() {
        window.addEventListener('resize', (/**
         * @return {?}
         */
        () => {
            this.headerHeight = this.scrollContent.clientHeight;
        }), false);
        if (this.scrollContent) {
            this.scrollContent.addEventListener('scroll', (/**
             * @param {?} e
             * @return {?}
             */
            (e) => {
                if (!this.ticking) {
                    window.requestAnimationFrame((/**
                     * @return {?}
                     */
                    () => {
                        this.updateElasticHeader();
                    }));
                }
                this.ticking = true;
            }));
        }
    }
    /**
     * @return {?}
     */
    updateElasticHeader() {
        if (!this.scrollContent || !toolbar) {
            return;
        }
        this.scrollTop = this.scrollContent.scrollTop;
        if (this.scrollTop >= 0) {
            this.translateAmt = this.scrollTop / 2;
            this.scaleAmt = 1;
        }
        else {
            this.translateAmt = 0;
            this.scaleAmt = -this.scrollTop / this.headerHeight + 1;
        }
        // Parallax total progress
        this.headerMinHeight = this.toolbar.offsetHeight;
        /** @type {?} */
        let progress = (this.maximumHeight - this.scrollTop - this.headerMinHeight) / (this.maximumHeight - this.headerMinHeight);
        progress = Math.max(progress, 0);
        // ion-header: set height
        /** @type {?} */
        let targetHeight = this.maximumHeight - this.scrollTop;
        targetHeight = Math.max(targetHeight, this.headerMinHeight);
        // .toolbar-background: change color
        this.renderer.setStyle(this.imageOverlay, 'height', `${targetHeight}px`);
        //this.renderer.setStyle(this.imageOverlay, 'opacity', `${progress}`);
        
        if (targetHeight <= (this.maximumHeight - 50)) {
            let titleOpacity = progress > 0.95 ? 0.8 : 1 - ((1 - 0.7) * progress);
            let fontSize = 2 + (2.4 * progress );
            this.renderer.setStyle(this.overlayTitle, 'font-size', `${fontSize}em`);
            this.renderer.setStyle(this.overlayTitle, 'background-color', `rgba(54, 65, 161, ${titleOpacity})`);
        } else {
            this.renderer.setStyle(this.overlayTitle, 'font-size', `4em`);
            this.renderer.setStyle(this.overlayTitle, 'background-color', `rgba(54, 65, 161, 0.80)`);
        }
        this.renderer.setStyle(this.colorOverlay, 'height', `${targetHeight}px`);
        this.renderer.setStyle(this.colorOverlay, 'opacity', targetHeight > this.headerMinHeight ? '1' : '0');
        this.renderer.setStyle(this.toolbarBackground, 'background-color', targetHeight > this.headerMinHeight ? 'transparent' : this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            if (targetHeight > this.headerMinHeight) {
                this.imageOverlay.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                btn => {
                    this.renderer.setStyle(btn, 'color', this.titleColor);
                }));
            }
            else {
                this.toolbar.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                btn => {
                    //this.renderer.setStyle(btn, 'color', 'unset');
                }));
            }
        }

        if (this.shareButtons) {
            if (targetHeight > this.headerMinHeight) {
                this.imageOverlay.append(this.shareButtons);
                Array.from(this.shareButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                btn => {
                    this.renderer.setStyle(btn, 'color', this.titleColor);
                }));
            }
            else {
                this.toolbar.append(this.shareButtons);
                Array.from(this.shareButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                btn => {
                    //this.renderer.setStyle(btn, 'color', 'unset');
                }));
            }
        }
        this.ticking = false;
    }
}
ParallaxDirective.ɵfac = function ParallaxDirective_Factory(t) { return new (t || ParallaxDirective)(ɵngcc0.ɵɵdirectiveInject(ɵngcc0.ElementRef), ɵngcc0.ɵɵdirectiveInject(ɵngcc0.Renderer2)); };
ParallaxDirective.ɵdir = ɵngcc0.ɵɵdefineDirective({ type: ParallaxDirective, selectors: [["ion-header", "parallax", ""]], inputs: { maximumHeight: "maximumHeight", imageUrl: "imageUrl", expandedColor: "expandedColor", titleColor: "titleColor" } });
/** @nocollapse */
ParallaxDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
ParallaxDirective.propDecorators = {
    imageUrl: [{ type: Input }],
    expandedColor: [{ type: Input }],
    titleColor: [{ type: Input }],
    maximumHeight: [{ type: Input }]
};
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(ParallaxDirective, [{
        type: Directive,
        args: [{
                selector: 'ion-header[parallax]'
            }]
    }], function () { return [{ type: ɵngcc0.ElementRef }, { type: ɵngcc0.Renderer2 }]; }, { maximumHeight: [{
            type: Input
        }], imageUrl: [{
            type: Input
        }], expandedColor: [{
            type: Input
        }], titleColor: [{
            type: Input
        }] }); })();
if (false) {
    /** @type {?} */
    ParallaxDirective.prototype.imageUrl;
    /** @type {?} */
    ParallaxDirective.prototype.expandedColor;
    /** @type {?} */
    ParallaxDirective.prototype.titleColor;
    /** @type {?} */
    ParallaxDirective.prototype.maximumHeight;
    /** @type {?} */
    ParallaxDirective.prototype.header;
    /** @type {?} */
    ParallaxDirective.prototype.toolbar;
    /** @type {?} */
    ParallaxDirective.prototype.toolbarBackground;
    /** @type {?} */
    ParallaxDirective.prototype.imageOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.colorOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.barButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContent;
    /** @type {?} */
    ParallaxDirective.prototype.headerHeight;
    /** @type {?} */
    ParallaxDirective.prototype.headerMinHeight;
    /** @type {?} */
    ParallaxDirective.prototype.translateAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scaleAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.lastScrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.ticking;
    /** @type {?} */
    ParallaxDirective.prototype.originalToolbarBgColor;
    /** @type {?} */
    ParallaxDirective.prototype.overlayTitle;
    /** @type {?} */
    ParallaxDirective.prototype.ionTitle;
    /** @type {?} */
    ParallaxDirective.prototype.overlayButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContentPaddingTop;
    /** @type {?} */
    ParallaxDirective.prototype.headerRef;
    /** @type {?} */
    ParallaxDirective.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class IonicHeaderParallaxModule {
}
IonicHeaderParallaxModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: IonicHeaderParallaxModule });
IonicHeaderParallaxModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function IonicHeaderParallaxModule_Factory(t) { return new (t || IonicHeaderParallaxModule)(); }, imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵngcc0.ɵɵsetNgModuleScope(IonicHeaderParallaxModule, { declarations: [ParallaxDirective], exports: [ParallaxDirective] }); })();
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(IonicHeaderParallaxModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    ParallaxDirective
                ],
                imports: [],
                exports: [
                    ParallaxDirective
                ]
            }]
    }], null, null); })();

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { IonicHeaderParallaxModule, ParallaxDirective };

//# sourceMappingURL=ionic-header-parallax.js.map