/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { ParallaxDirective } from './parallax.directive';
export class IonicHeaderParallaxModule {
}
IonicHeaderParallaxModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    ParallaxDirective
                ],
                imports: [],
                exports: [
                    ParallaxDirective
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtaGVhZGVyLXBhcmFsbGF4Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2lvbmljLWhlYWRlci1wYXJhbGxheC8iLCJzb3VyY2VzIjpbImxpYi9pb25pYy1oZWFkZXItcGFyYWxsYXgubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBWXpELE1BQU0sT0FBTyx5QkFBeUI7OztZQVZyQyxRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLGlCQUFpQjtpQkFDbEI7Z0JBQ0QsT0FBTyxFQUFFLEVBQ1I7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLGlCQUFpQjtpQkFDbEI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBhcmFsbGF4RGlyZWN0aXZlIH0gZnJvbSAnLi9wYXJhbGxheC5kaXJlY3RpdmUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIFBhcmFsbGF4RGlyZWN0aXZlXHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBQYXJhbGxheERpcmVjdGl2ZVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIElvbmljSGVhZGVyUGFyYWxsYXhNb2R1bGUgeyB9XHJcbiJdfQ==