/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
export class ParallaxDirective {
    /**
     * @param {?} headerRef
     * @param {?} renderer
     */
    constructor(headerRef, renderer) {
        this.headerRef = headerRef;
        this.renderer = renderer;
        this.maximumHeight = 300;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.initElements();
            this.initStyles();
            this.initEvents();
        }), 100);
    }
    /**
     * @return {?}
     */
    initElements() {
        /** @type {?} */
        const parentElement = this.headerRef.nativeElement.parentElement;
        this.header = this.headerRef.nativeElement;
        this.toolbar = this.header.querySelector('ion-toolbar');
        if (!this.toolbar) {
            throw new Error('Parallax directive requires a toolbar or navbar element on the page to work.');
        }
        this.ionTitle = this.toolbar.querySelector('ion-title');
        this.toolbarBackground = this.toolbar.shadowRoot.querySelector('.toolbar-background');
        this.barButtons = this.headerRef.nativeElement.querySelector('ion-buttons');
        /** @type {?} */
        const ionContent = parentElement.querySelector('ion-content');
        this.scrollContent = ionContent.shadowRoot.querySelector('.inner-scroll');
        if (!this.scrollContent) {
            throw new Error('Parallax directive requires an <ion-content> element on the page to work.');
        }
        // Create image overlay
        this.imageOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.imageOverlay, 'image-overlay');
        this.colorOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.colorOverlay, 'color-overlay');
        this.colorOverlay.appendChild(this.imageOverlay);
        this.header.appendChild(this.colorOverlay);
        // Copy title and buttons
        this.overlayTitle = this.ionTitle && (/** @type {?} */ (this.ionTitle.cloneNode(true)));
        if (this.overlayTitle) {
            this.renderer.addClass(this.overlayTitle, 'parallax-title');
            setTimeout((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const toolbarTitle = this.overlayTitle.shadowRoot.querySelector('.toolbar-title');
                this.renderer.setStyle(toolbarTitle, 'pointer-events', 'unset');
            }));
        }
        if (this.overlayTitle) {
            this.imageOverlay.appendChild(this.overlayTitle);
        }
        if (this.barButtons) {
            this.imageOverlay.appendChild(this.barButtons);
        }
    }
    /**
     * @return {?}
     */
    initStyles() {
        this.headerHeight = this.scrollContent.clientHeight;
        this.ticking = false;
        if (!this.scrollContent || !toolbar) {
            return;
        }
        // fetch styles
        this.maximumHeight = parseFloat(this.maximumHeight.toString());
        this.headerMinHeight = this.toolbar.offsetHeight;
        this.scrollContentPaddingTop = window.getComputedStyle(this.scrollContent, null).paddingTop.replace('px', '');
        this.scrollContentPaddingTop = parseFloat(this.scrollContentPaddingTop);
        this.originalToolbarBgColor = window.getComputedStyle(this.toolbarBackground, null).backgroundColor;
        // header and title
        this.renderer.setStyle(this.header, 'position', 'relative');
        if (this.overlayTitle) {
            this.renderer.setStyle(this.overlayTitle, 'color', this.titleColor);
            this.renderer.setStyle(this.overlayTitle, 'position', 'absolute');
            this.renderer.setStyle(this.overlayTitle, 'width', '100%');
            this.renderer.setStyle(this.overlayTitle, 'height', '100%');
            this.renderer.setStyle(this.overlayTitle, 'text-align', 'center');
        }
        // color overlay
        this.renderer.setStyle(this.colorOverlay, 'background-color', this.originalToolbarBgColor);
        this.renderer.setStyle(this.colorOverlay, 'height', `${this.maximumHeight}px`);
        this.renderer.setStyle(this.colorOverlay, 'position', 'absolute');
        this.renderer.setStyle(this.colorOverlay, 'top', `${-this.headerMinHeight * 0}px`);
        this.renderer.setStyle(this.colorOverlay, 'left', '0');
        this.renderer.setStyle(this.colorOverlay, 'width', '100%');
        this.renderer.setStyle(this.colorOverlay, 'z-index', '10');
        this.renderer.setStyle(this.colorOverlay, 'pointer-events', 'none');
        // image overlay
        this.renderer.setStyle(this.imageOverlay, 'background-color', this.expandedColor);
        this.renderer.setStyle(this.imageOverlay, 'background-image', `url(${this.imageUrl || ''})`);
        this.renderer.setStyle(this.imageOverlay, 'height', `100%`);
        this.renderer.setStyle(this.imageOverlay, 'width', '100%');
        this.renderer.setStyle(this.imageOverlay, 'pointer-events', 'none');
        this.renderer.setStyle(this.imageOverlay, 'background-size', 'cover');
        this.renderer.setStyle(this.imageOverlay, 'background-position', 'center');
        // .toolbar-background
        this.renderer.setStyle(this.toolbarBackground, 'background-color', this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            this.renderer.setStyle(this.barButtons, 'pointer-events', 'all');
            Array.from(this.barButtons.children).forEach((/**
             * @param {?} btn
             * @return {?}
             */
            btn => {
                this.renderer.setStyle(btn, 'color', this.titleColor);
            }));
        }
        // .scroll-content
        if (this.scrollContent) {
            this.renderer.setAttribute(this.scrollContent, 'parallax', '');
            this.renderer.setStyle(this.scrollContent, 'padding-top', `${this.maximumHeight + this.scrollContentPaddingTop - this.headerMinHeight}px`);
        }
    }
    /**
     * @return {?}
     */
    initEvents() {
        window.addEventListener('resize', (/**
         * @return {?}
         */
        () => {
            this.headerHeight = this.scrollContent.clientHeight;
        }), false);
        if (this.scrollContent) {
            this.scrollContent.addEventListener('scroll', (/**
             * @param {?} e
             * @return {?}
             */
            (e) => {
                if (!this.ticking) {
                    window.requestAnimationFrame((/**
                     * @return {?}
                     */
                    () => {
                        this.updateElasticHeader();
                    }));
                }
                this.ticking = true;
            }));
        }
    }
    /**
     * @return {?}
     */
    updateElasticHeader() {
        if (!this.scrollContent || !toolbar) {
            return;
        }
        this.scrollTop = this.scrollContent.scrollTop;
        if (this.scrollTop >= 0) {
            this.translateAmt = this.scrollTop / 2;
            this.scaleAmt = 1;
        }
        else {
            this.translateAmt = 0;
            this.scaleAmt = -this.scrollTop / this.headerHeight + 1;
        }
        // Parallax total progress
        this.headerMinHeight = this.toolbar.offsetHeight;
        /** @type {?} */
        let progress = (this.maximumHeight - this.scrollTop - this.headerMinHeight) / (this.maximumHeight - this.headerMinHeight);
        progress = Math.max(progress, 0);
        // ion-header: set height
        /** @type {?} */
        let targetHeight = this.maximumHeight - this.scrollTop;
        targetHeight = Math.max(targetHeight, this.headerMinHeight);
        // .toolbar-background: change color
        console.log(progress);
        this.renderer.setStyle(this.imageOverlay, 'height', `${targetHeight}px`);
        this.renderer.setStyle(this.imageOverlay, 'opacity', `${progress}`);
        this.renderer.setStyle(this.colorOverlay, 'height', `${targetHeight}px`);
        this.renderer.setStyle(this.colorOverlay, 'opacity', targetHeight > this.headerMinHeight ? '1' : '0');
        this.renderer.setStyle(this.toolbarBackground, 'background-color', targetHeight > this.headerMinHeight ? 'transparent' : this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            if (targetHeight > this.headerMinHeight) {
                this.imageOverlay.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                btn => {
                    this.renderer.setStyle(btn, 'color', this.titleColor);
                }));
            }
            else {
                this.toolbar.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                btn => {
                    this.renderer.setStyle(btn, 'color', 'unset');
                }));
            }
        }
        this.ticking = false;
    }
}
ParallaxDirective.decorators = [
    { type: Directive, args: [{
                selector: 'ion-header[parallax]'
            },] }
];
/** @nocollapse */
ParallaxDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
ParallaxDirective.propDecorators = {
    imageUrl: [{ type: Input }],
    expandedColor: [{ type: Input }],
    titleColor: [{ type: Input }],
    maximumHeight: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    ParallaxDirective.prototype.imageUrl;
    /** @type {?} */
    ParallaxDirective.prototype.expandedColor;
    /** @type {?} */
    ParallaxDirective.prototype.titleColor;
    /** @type {?} */
    ParallaxDirective.prototype.maximumHeight;
    /** @type {?} */
    ParallaxDirective.prototype.header;
    /** @type {?} */
    ParallaxDirective.prototype.toolbar;
    /** @type {?} */
    ParallaxDirective.prototype.toolbarBackground;
    /** @type {?} */
    ParallaxDirective.prototype.imageOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.colorOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.barButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContent;
    /** @type {?} */
    ParallaxDirective.prototype.headerHeight;
    /** @type {?} */
    ParallaxDirective.prototype.headerMinHeight;
    /** @type {?} */
    ParallaxDirective.prototype.translateAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scaleAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.lastScrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.ticking;
    /** @type {?} */
    ParallaxDirective.prototype.originalToolbarBgColor;
    /** @type {?} */
    ParallaxDirective.prototype.overlayTitle;
    /** @type {?} */
    ParallaxDirective.prototype.ionTitle;
    /** @type {?} */
    ParallaxDirective.prototype.overlayButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContentPaddingTop;
    /** @type {?} */
    ParallaxDirective.prototype.headerRef;
    /** @type {?} */
    ParallaxDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFyYWxsYXguZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaW9uaWMtaGVhZGVyLXBhcmFsbGF4LyIsInNvdXJjZXMiOlsibGliL3BhcmFsbGF4LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFLdkYsTUFBTSxPQUFPLGlCQUFpQjs7Ozs7SUEwQjVCLFlBQW1CLFNBQWtDLEVBQVMsUUFBbUI7UUFBOUQsY0FBUyxHQUFULFNBQVMsQ0FBeUI7UUFBUyxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBdEJ4RSxrQkFBYSxHQUFHLEdBQUcsQ0FBQztJQXVCN0IsQ0FBQzs7OztJQUVELGVBQWU7UUFDYixVQUFVOzs7UUFBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNwQixDQUFDLEdBQUUsR0FBRyxDQUFDLENBQUM7SUFDVixDQUFDOzs7O0lBRUQsWUFBWTs7Y0FDSixhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsYUFBYTtRQUNoRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDO1FBRTNDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLDhFQUE4RSxDQUFDLENBQUM7U0FBRTtRQUN2SCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUV0RixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7Y0FDdEUsVUFBVSxHQUFHLGFBQWEsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDO1FBQzdELElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLDJFQUEyRSxDQUFDLENBQUM7U0FBRTtRQUUxSCx1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBRTNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxlQUFlLENBQUMsQ0FBQztRQUUzRCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRTNDLHlCQUF5QjtRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLElBQUksbUJBQUEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQWUsQ0FBQztRQUNsRixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzVELFVBQVU7OztZQUFDLEdBQUcsRUFBRTs7c0JBQ1IsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDakYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2xFLENBQUMsRUFBQyxDQUFDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FBRTtRQUM1RSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FBRTtJQUMxRSxDQUFDOzs7O0lBRUQsVUFBVTtRQUNSLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDcEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFFckIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFBRSxPQUFPO1NBQUU7UUFFaEQsZUFBZTtRQUNmLElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDO1FBQ2pELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUM5RyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDLGVBQWUsQ0FBQztRQUVwRyxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDNUQsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNwRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNsRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUM1RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFlBQVksRUFBRSxRQUFRLENBQUMsQ0FBQztTQUNuRTtRQUVELGdCQUFnQjtRQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLGtCQUFrQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQzNGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxFQUFFLEdBQUcsSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBRXBFLGdCQUFnQjtRQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLGtCQUFrQixFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLGtCQUFrQixFQUFFLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzdGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxpQkFBaUIsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLHFCQUFxQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRTNFLHNCQUFzQjtRQUN0QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFFaEcsZUFBZTtRQUNmLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ2pFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPOzs7O1lBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hELENBQUMsRUFBQyxDQUFDO1NBQ0o7UUFFRCxrQkFBa0I7UUFDbEIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQy9ELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsYUFBYSxFQUN0RCxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxDQUFDO1NBQ3BGO0lBQ0gsQ0FBQzs7OztJQUVELFVBQVU7UUFDUixNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUTs7O1FBQUUsR0FBRyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDdEQsQ0FBQyxHQUFFLEtBQUssQ0FBQyxDQUFDO1FBRVYsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsUUFBUTs7OztZQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBRWxELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNqQixNQUFNLENBQUMscUJBQXFCOzs7b0JBQUMsR0FBRyxFQUFFO3dCQUNoQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztvQkFDN0IsQ0FBQyxFQUFDLENBQUM7aUJBQ0o7Z0JBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDdEIsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7SUFFRCxtQkFBbUI7UUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFBRSxPQUFPO1NBQUU7UUFFaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQztRQUM5QyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7U0FDbkI7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1NBQ3pEO1FBRUQsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7O1lBQzdDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDekgsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDOzs7WUFHN0IsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVM7UUFDdEQsWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUU1RCxvQ0FBb0M7UUFDcEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsR0FBRyxZQUFZLElBQUksQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsU0FBUyxFQUFFLEdBQUcsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBRSxHQUFHLFlBQVksSUFBSSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLGtCQUFrQixFQUMvRCxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUVyRixlQUFlO1FBQ2YsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3ZDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDMUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU87Ozs7Z0JBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN4RCxDQUFDLEVBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDckMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU87Ozs7Z0JBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ2hELENBQUMsRUFBQyxDQUFDO2FBQ0o7U0FDRjtRQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7OztZQTFNRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjthQUNqQzs7OztZQUptQixVQUFVO1lBQVMsU0FBUzs7O3VCQU03QyxLQUFLOzRCQUNMLEtBQUs7eUJBQ0wsS0FBSzs0QkFDTCxLQUFLOzs7O0lBSE4scUNBQTBCOztJQUMxQiwwQ0FBK0I7O0lBQy9CLHVDQUE0Qjs7SUFDNUIsMENBQTZCOztJQUU3QixtQ0FBb0I7O0lBQ3BCLG9DQUFxQjs7SUFDckIsOENBQStCOztJQUMvQix5Q0FBMEI7O0lBQzFCLHlDQUEwQjs7SUFDMUIsdUNBQXdCOztJQUN4QiwwQ0FBMkI7O0lBQzNCLHlDQUFrQjs7SUFDbEIsNENBQXdCOztJQUN4Qix5Q0FBa0I7O0lBQ2xCLHFDQUFjOztJQUNkLHNDQUFlOztJQUNmLDBDQUFtQjs7SUFDbkIsb0NBQWE7O0lBQ2IsbURBQStCOztJQUMvQix5Q0FBMEI7O0lBQzFCLHFDQUFzQjs7SUFDdEIsMkNBQThCOztJQUM5QixvREFBd0I7O0lBRVosc0NBQXlDOztJQUFFLHFDQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSW5wdXQsIFJlbmRlcmVyMiwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdpb24taGVhZGVyW3BhcmFsbGF4XSdcclxufSlcclxuZXhwb3J0IGNsYXNzIFBhcmFsbGF4RGlyZWN0aXZlIGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgQElucHV0KCkgaW1hZ2VVcmw6IHN0cmluZztcclxuICBASW5wdXQoKSBleHBhbmRlZENvbG9yOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgdGl0bGVDb2xvcjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1heGltdW1IZWlnaHQgPSAzMDA7XHJcblxyXG4gIGhlYWRlcjogSFRNTEVsZW1lbnQ7XHJcbiAgdG9vbGJhcjogSFRNTEVsZW1lbnQ7XHJcbiAgdG9vbGJhckJhY2tncm91bmQ6IEhUTUxFbGVtZW50O1xyXG4gIGltYWdlT3ZlcmxheTogSFRNTEVsZW1lbnQ7XHJcbiAgY29sb3JPdmVybGF5OiBIVE1MRWxlbWVudDtcclxuICBiYXJCdXR0b25zOiBIVE1MRWxlbWVudDtcclxuICBzY3JvbGxDb250ZW50OiBIVE1MRWxlbWVudDtcclxuICBoZWFkZXJIZWlnaHQ6IGFueTtcclxuICBoZWFkZXJNaW5IZWlnaHQ6IG51bWJlcjtcclxuICB0cmFuc2xhdGVBbXQ6IGFueTtcclxuICBzY2FsZUFtdDogYW55O1xyXG4gIHNjcm9sbFRvcDogYW55O1xyXG4gIGxhc3RTY3JvbGxUb3A6IGFueTtcclxuICB0aWNraW5nOiBhbnk7XHJcbiAgb3JpZ2luYWxUb29sYmFyQmdDb2xvcjogc3RyaW5nO1xyXG4gIG92ZXJsYXlUaXRsZTogSFRNTEVsZW1lbnQ7XHJcbiAgaW9uVGl0bGU6IEhUTUxFbGVtZW50O1xyXG4gIG92ZXJsYXlCdXR0b25zOiBIVE1MRWxlbWVudFtdO1xyXG4gIHNjcm9sbENvbnRlbnRQYWRkaW5nVG9wO1xyXG5cclxuICBjb25zdHJ1Y3RvcihwdWJsaWMgaGVhZGVyUmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PiwgcHVibGljIHJlbmRlcmVyOiBSZW5kZXJlcjIpIHtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICB0aGlzLmluaXRFbGVtZW50cygpO1xyXG4gICAgICB0aGlzLmluaXRTdHlsZXMoKTtcclxuICAgICAgdGhpcy5pbml0RXZlbnRzKCk7XHJcbiAgICB9LCAxMDApO1xyXG4gIH1cclxuXHJcbiAgaW5pdEVsZW1lbnRzKCkge1xyXG4gICAgY29uc3QgcGFyZW50RWxlbWVudCA9IHRoaXMuaGVhZGVyUmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50RWxlbWVudDtcclxuICAgIHRoaXMuaGVhZGVyID0gdGhpcy5oZWFkZXJSZWYubmF0aXZlRWxlbWVudDtcclxuXHJcbiAgICB0aGlzLnRvb2xiYXIgPSB0aGlzLmhlYWRlci5xdWVyeVNlbGVjdG9yKCdpb24tdG9vbGJhcicpO1xyXG4gICAgaWYgKCF0aGlzLnRvb2xiYXIpIHsgdGhyb3cgbmV3IEVycm9yKCdQYXJhbGxheCBkaXJlY3RpdmUgcmVxdWlyZXMgYSB0b29sYmFyIG9yIG5hdmJhciBlbGVtZW50IG9uIHRoZSBwYWdlIHRvIHdvcmsuJyk7IH1cclxuICAgIHRoaXMuaW9uVGl0bGUgPSB0aGlzLnRvb2xiYXIucXVlcnlTZWxlY3RvcignaW9uLXRpdGxlJyk7XHJcbiAgICB0aGlzLnRvb2xiYXJCYWNrZ3JvdW5kID0gdGhpcy50b29sYmFyLnNoYWRvd1Jvb3QucXVlcnlTZWxlY3RvcignLnRvb2xiYXItYmFja2dyb3VuZCcpO1xyXG5cclxuICAgIHRoaXMuYmFyQnV0dG9ucyA9IHRoaXMuaGVhZGVyUmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW9uLWJ1dHRvbnMnKTtcclxuICAgIGNvbnN0IGlvbkNvbnRlbnQgPSBwYXJlbnRFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2lvbi1jb250ZW50Jyk7XHJcbiAgICB0aGlzLnNjcm9sbENvbnRlbnQgPSBpb25Db250ZW50LnNoYWRvd1Jvb3QucXVlcnlTZWxlY3RvcignLmlubmVyLXNjcm9sbCcpO1xyXG4gICAgaWYgKCF0aGlzLnNjcm9sbENvbnRlbnQpIHsgdGhyb3cgbmV3IEVycm9yKCdQYXJhbGxheCBkaXJlY3RpdmUgcmVxdWlyZXMgYW4gPGlvbi1jb250ZW50PiBlbGVtZW50IG9uIHRoZSBwYWdlIHRvIHdvcmsuJyk7IH1cclxuXHJcbiAgICAvLyBDcmVhdGUgaW1hZ2Ugb3ZlcmxheVxyXG4gICAgdGhpcy5pbWFnZU92ZXJsYXkgPSB0aGlzLnJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLmltYWdlT3ZlcmxheSwgJ2ltYWdlLW92ZXJsYXknKTtcclxuXHJcbiAgICB0aGlzLmNvbG9yT3ZlcmxheSA9IHRoaXMucmVuZGVyZXIuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuY29sb3JPdmVybGF5LCAnY29sb3Itb3ZlcmxheScpO1xyXG5cclxuICAgIHRoaXMuY29sb3JPdmVybGF5LmFwcGVuZENoaWxkKHRoaXMuaW1hZ2VPdmVybGF5KTtcclxuICAgIHRoaXMuaGVhZGVyLmFwcGVuZENoaWxkKHRoaXMuY29sb3JPdmVybGF5KTtcclxuXHJcbiAgICAvLyBDb3B5IHRpdGxlIGFuZCBidXR0b25zXHJcbiAgICB0aGlzLm92ZXJsYXlUaXRsZSA9IHRoaXMuaW9uVGl0bGUgJiYgdGhpcy5pb25UaXRsZS5jbG9uZU5vZGUodHJ1ZSkgYXMgSFRNTEVsZW1lbnQ7XHJcbiAgICBpZiAodGhpcy5vdmVybGF5VGl0bGUpIHtcclxuICAgICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLm92ZXJsYXlUaXRsZSwgJ3BhcmFsbGF4LXRpdGxlJyk7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHRvb2xiYXJUaXRsZSA9IHRoaXMub3ZlcmxheVRpdGxlLnNoYWRvd1Jvb3QucXVlcnlTZWxlY3RvcignLnRvb2xiYXItdGl0bGUnKTtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRvb2xiYXJUaXRsZSwgJ3BvaW50ZXItZXZlbnRzJywgJ3Vuc2V0Jyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLm92ZXJsYXlUaXRsZSkgeyB0aGlzLmltYWdlT3ZlcmxheS5hcHBlbmRDaGlsZCh0aGlzLm92ZXJsYXlUaXRsZSk7IH1cclxuICAgIGlmICh0aGlzLmJhckJ1dHRvbnMpIHsgdGhpcy5pbWFnZU92ZXJsYXkuYXBwZW5kQ2hpbGQodGhpcy5iYXJCdXR0b25zKTsgfVxyXG4gIH1cclxuXHJcbiAgaW5pdFN0eWxlcygpIHtcclxuICAgIHRoaXMuaGVhZGVySGVpZ2h0ID0gdGhpcy5zY3JvbGxDb250ZW50LmNsaWVudEhlaWdodDtcclxuICAgIHRoaXMudGlja2luZyA9IGZhbHNlO1xyXG5cclxuICAgIGlmICghdGhpcy5zY3JvbGxDb250ZW50IHx8ICF0b29sYmFyKSB7IHJldHVybjsgfVxyXG5cclxuICAgIC8vIGZldGNoIHN0eWxlc1xyXG4gICAgdGhpcy5tYXhpbXVtSGVpZ2h0ID0gcGFyc2VGbG9hdCh0aGlzLm1heGltdW1IZWlnaHQudG9TdHJpbmcoKSk7XHJcbiAgICB0aGlzLmhlYWRlck1pbkhlaWdodCA9IHRoaXMudG9vbGJhci5vZmZzZXRIZWlnaHQ7XHJcbiAgICB0aGlzLnNjcm9sbENvbnRlbnRQYWRkaW5nVG9wID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUodGhpcy5zY3JvbGxDb250ZW50LCBudWxsKS5wYWRkaW5nVG9wLnJlcGxhY2UoJ3B4JywgJycpO1xyXG4gICAgdGhpcy5zY3JvbGxDb250ZW50UGFkZGluZ1RvcCA9IHBhcnNlRmxvYXQodGhpcy5zY3JvbGxDb250ZW50UGFkZGluZ1RvcCk7XHJcbiAgICB0aGlzLm9yaWdpbmFsVG9vbGJhckJnQ29sb3IgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLnRvb2xiYXJCYWNrZ3JvdW5kLCBudWxsKS5iYWNrZ3JvdW5kQ29sb3I7XHJcblxyXG4gICAgLy8gaGVhZGVyIGFuZCB0aXRsZVxyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmhlYWRlciwgJ3Bvc2l0aW9uJywgJ3JlbGF0aXZlJyk7XHJcbiAgICBpZiAodGhpcy5vdmVybGF5VGl0bGUpIHtcclxuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLm92ZXJsYXlUaXRsZSwgJ2NvbG9yJywgdGhpcy50aXRsZUNvbG9yKTtcclxuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLm92ZXJsYXlUaXRsZSwgJ3Bvc2l0aW9uJywgJ2Fic29sdXRlJyk7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5vdmVybGF5VGl0bGUsICd3aWR0aCcsICcxMDAlJyk7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5vdmVybGF5VGl0bGUsICdoZWlnaHQnLCAnMTAwJScpO1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMub3ZlcmxheVRpdGxlLCAndGV4dC1hbGlnbicsICdjZW50ZXInKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBjb2xvciBvdmVybGF5XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY29sb3JPdmVybGF5LCAnYmFja2dyb3VuZC1jb2xvcicsIHRoaXMub3JpZ2luYWxUb29sYmFyQmdDb2xvcik7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY29sb3JPdmVybGF5LCAnaGVpZ2h0JywgYCR7dGhpcy5tYXhpbXVtSGVpZ2h0fXB4YCk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY29sb3JPdmVybGF5LCAncG9zaXRpb24nLCAnYWJzb2x1dGUnKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5jb2xvck92ZXJsYXksICd0b3AnLCBgJHstdGhpcy5oZWFkZXJNaW5IZWlnaHQgKiAwfXB4YCk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY29sb3JPdmVybGF5LCAnbGVmdCcsICcwJyk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY29sb3JPdmVybGF5LCAnd2lkdGgnLCAnMTAwJScpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ3otaW5kZXgnLCAnMTAnKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5jb2xvck92ZXJsYXksICdwb2ludGVyLWV2ZW50cycsICdub25lJyk7XHJcblxyXG4gICAgLy8gaW1hZ2Ugb3ZlcmxheVxyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ2JhY2tncm91bmQtY29sb3InLCB0aGlzLmV4cGFuZGVkQ29sb3IpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ2JhY2tncm91bmQtaW1hZ2UnLCBgdXJsKCR7dGhpcy5pbWFnZVVybCB8fCAnJ30pYCk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuaW1hZ2VPdmVybGF5LCAnaGVpZ2h0JywgYDEwMCVgKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5pbWFnZU92ZXJsYXksICd3aWR0aCcsICcxMDAlJyk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuaW1hZ2VPdmVybGF5LCAncG9pbnRlci1ldmVudHMnLCAnbm9uZScpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ2JhY2tncm91bmQtc2l6ZScsICdjb3ZlcicpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ2JhY2tncm91bmQtcG9zaXRpb24nLCAnY2VudGVyJyk7XHJcblxyXG4gICAgLy8gLnRvb2xiYXItYmFja2dyb3VuZFxyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLnRvb2xiYXJCYWNrZ3JvdW5kLCAnYmFja2dyb3VuZC1jb2xvcicsIHRoaXMub3JpZ2luYWxUb29sYmFyQmdDb2xvcik7XHJcblxyXG4gICAgLy8gLmJhci1idXR0b25zXHJcbiAgICBpZiAodGhpcy5iYXJCdXR0b25zKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5iYXJCdXR0b25zLCAncG9pbnRlci1ldmVudHMnLCAnYWxsJyk7XHJcbiAgICAgIEFycmF5LmZyb20odGhpcy5iYXJCdXR0b25zLmNoaWxkcmVuKS5mb3JFYWNoKGJ0biA9PiB7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShidG4sICdjb2xvcicsIHRoaXMudGl0bGVDb2xvcik7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIC5zY3JvbGwtY29udGVudFxyXG4gICAgaWYgKHRoaXMuc2Nyb2xsQ29udGVudCkge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldEF0dHJpYnV0ZSh0aGlzLnNjcm9sbENvbnRlbnQsICdwYXJhbGxheCcsICcnKTtcclxuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLnNjcm9sbENvbnRlbnQsICdwYWRkaW5nLXRvcCcsXHJcbiAgICAgICAgYCR7dGhpcy5tYXhpbXVtSGVpZ2h0ICsgdGhpcy5zY3JvbGxDb250ZW50UGFkZGluZ1RvcCAtIHRoaXMuaGVhZGVyTWluSGVpZ2h0fXB4YCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBpbml0RXZlbnRzKCkge1xyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsICgpID0+IHtcclxuICAgICAgdGhpcy5oZWFkZXJIZWlnaHQgPSB0aGlzLnNjcm9sbENvbnRlbnQuY2xpZW50SGVpZ2h0O1xyXG4gICAgfSwgZmFsc2UpO1xyXG5cclxuICAgIGlmICh0aGlzLnNjcm9sbENvbnRlbnQpIHtcclxuICAgICAgdGhpcy5zY3JvbGxDb250ZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIChlKSA9PiB7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy50aWNraW5nKSB7XHJcbiAgICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVFbGFzdGljSGVhZGVyKCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy50aWNraW5nID0gdHJ1ZTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB1cGRhdGVFbGFzdGljSGVhZGVyKCkge1xyXG4gICAgaWYgKCF0aGlzLnNjcm9sbENvbnRlbnQgfHwgIXRvb2xiYXIpIHsgcmV0dXJuOyB9XHJcblxyXG4gICAgdGhpcy5zY3JvbGxUb3AgPSB0aGlzLnNjcm9sbENvbnRlbnQuc2Nyb2xsVG9wO1xyXG4gICAgaWYgKHRoaXMuc2Nyb2xsVG9wID49IDApIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVBbXQgPSB0aGlzLnNjcm9sbFRvcCAvIDI7XHJcbiAgICAgIHRoaXMuc2NhbGVBbXQgPSAxO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVBbXQgPSAwO1xyXG4gICAgICB0aGlzLnNjYWxlQW10ID0gLXRoaXMuc2Nyb2xsVG9wIC8gdGhpcy5oZWFkZXJIZWlnaHQgKyAxO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFBhcmFsbGF4IHRvdGFsIHByb2dyZXNzXHJcbiAgICB0aGlzLmhlYWRlck1pbkhlaWdodCA9IHRoaXMudG9vbGJhci5vZmZzZXRIZWlnaHQ7XHJcbiAgICBsZXQgcHJvZ3Jlc3MgPSAodGhpcy5tYXhpbXVtSGVpZ2h0IC0gdGhpcy5zY3JvbGxUb3AgLSB0aGlzLmhlYWRlck1pbkhlaWdodCkgLyAodGhpcy5tYXhpbXVtSGVpZ2h0IC0gdGhpcy5oZWFkZXJNaW5IZWlnaHQpO1xyXG4gICAgcHJvZ3Jlc3MgPSBNYXRoLm1heChwcm9ncmVzcywgMCk7XHJcblxyXG4gICAgLy8gaW9uLWhlYWRlcjogc2V0IGhlaWdodFxyXG4gICAgbGV0IHRhcmdldEhlaWdodCA9IHRoaXMubWF4aW11bUhlaWdodCAtIHRoaXMuc2Nyb2xsVG9wO1xyXG4gICAgdGFyZ2V0SGVpZ2h0ID0gTWF0aC5tYXgodGFyZ2V0SGVpZ2h0LCB0aGlzLmhlYWRlck1pbkhlaWdodCk7XHJcblxyXG4gICAgLy8gLnRvb2xiYXItYmFja2dyb3VuZDogY2hhbmdlIGNvbG9yXHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuaW1hZ2VPdmVybGF5LCAnaGVpZ2h0JywgYCR7dGFyZ2V0SGVpZ2h0fXB4YCk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuaW1hZ2VPdmVybGF5LCAnb3BhY2l0eScsIGAke3Byb2dyZXNzfWApO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ2hlaWdodCcsIGAke3RhcmdldEhlaWdodH1weGApO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ29wYWNpdHknLCB0YXJnZXRIZWlnaHQgPiB0aGlzLmhlYWRlck1pbkhlaWdodCA/ICcxJyA6ICcwJyk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMudG9vbGJhckJhY2tncm91bmQsICdiYWNrZ3JvdW5kLWNvbG9yJyxcclxuICAgICAgdGFyZ2V0SGVpZ2h0ID4gdGhpcy5oZWFkZXJNaW5IZWlnaHQgPyAndHJhbnNwYXJlbnQnIDogdGhpcy5vcmlnaW5hbFRvb2xiYXJCZ0NvbG9yKTtcclxuXHJcbiAgICAvLyAuYmFyLWJ1dHRvbnNcclxuICAgIGlmICh0aGlzLmJhckJ1dHRvbnMpIHtcclxuICAgICAgaWYgKHRhcmdldEhlaWdodCA+IHRoaXMuaGVhZGVyTWluSGVpZ2h0KSB7XHJcbiAgICAgICAgdGhpcy5pbWFnZU92ZXJsYXkuYXBwZW5kKHRoaXMuYmFyQnV0dG9ucyk7XHJcbiAgICAgICAgQXJyYXkuZnJvbSh0aGlzLmJhckJ1dHRvbnMuY2hpbGRyZW4pLmZvckVhY2goYnRuID0+IHtcclxuICAgICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoYnRuLCAnY29sb3InLCB0aGlzLnRpdGxlQ29sb3IpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMudG9vbGJhci5hcHBlbmQodGhpcy5iYXJCdXR0b25zKTtcclxuICAgICAgICBBcnJheS5mcm9tKHRoaXMuYmFyQnV0dG9ucy5jaGlsZHJlbikuZm9yRWFjaChidG4gPT4ge1xyXG4gICAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShidG4sICdjb2xvcicsICd1bnNldCcpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy50aWNraW5nID0gZmFsc2U7XHJcbiAgfVxyXG59XHJcbiJdfQ==