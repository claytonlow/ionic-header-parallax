import { Directive, ElementRef, Renderer2, Input, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ParallaxDirective = /** @class */ (function () {
    function ParallaxDirective(headerRef, renderer) {
        this.headerRef = headerRef;
        this.renderer = renderer;
        this.maximumHeight = 300;
    }
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () {
            _this.initElements();
            _this.initStyles();
            _this.initEvents();
        }), 100);
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.initElements = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var parentElement = this.headerRef.nativeElement.parentElement;
        this.header = this.headerRef.nativeElement;
        this.toolbar = this.header.querySelector('ion-toolbar');
        if (!this.toolbar) {
            throw new Error('Parallax directive requires a toolbar or navbar element on the page to work.');
        }
        this.ionTitle = this.toolbar.querySelector('ion-title');
        this.toolbarBackground = this.toolbar.shadowRoot.querySelector('.toolbar-background');
        this.barButtons = this.headerRef.nativeElement.querySelector('ion-buttons');
        /** @type {?} */
        var ionContent = parentElement.querySelector('ion-content');
        this.scrollContent = ionContent.shadowRoot.querySelector('.inner-scroll');
        if (!this.scrollContent) {
            throw new Error('Parallax directive requires an <ion-content> element on the page to work.');
        }
        // Create image overlay
        this.imageOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.imageOverlay, 'image-overlay');
        this.colorOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.colorOverlay, 'color-overlay');
        this.colorOverlay.appendChild(this.imageOverlay);
        this.header.appendChild(this.colorOverlay);
        // Copy title and buttons
        this.overlayTitle = this.ionTitle && (/** @type {?} */ (this.ionTitle.cloneNode(true)));
        if (this.overlayTitle) {
            this.renderer.addClass(this.overlayTitle, 'parallax-title');
            setTimeout((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var toolbarTitle = _this.overlayTitle.shadowRoot.querySelector('.toolbar-title');
                _this.renderer.setStyle(toolbarTitle, 'pointer-events', 'unset');
            }));
        }
        if (this.overlayTitle) {
            this.imageOverlay.appendChild(this.overlayTitle);
        }
        if (this.barButtons) {
            this.imageOverlay.appendChild(this.barButtons);
        }
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.initStyles = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.headerHeight = this.scrollContent.clientHeight;
        this.ticking = false;
        if (!this.scrollContent || !toolbar) {
            return;
        }
        // fetch styles
        this.maximumHeight = parseFloat(this.maximumHeight.toString());
        this.headerMinHeight = this.toolbar.offsetHeight;
        this.scrollContentPaddingTop = window.getComputedStyle(this.scrollContent, null).paddingTop.replace('px', '');
        this.scrollContentPaddingTop = parseFloat(this.scrollContentPaddingTop);
        this.originalToolbarBgColor = window.getComputedStyle(this.toolbarBackground, null).backgroundColor;
        // header and title
        this.renderer.setStyle(this.header, 'position', 'relative');
        if (this.overlayTitle) {
            this.renderer.setStyle(this.overlayTitle, 'color', this.titleColor);
            this.renderer.setStyle(this.overlayTitle, 'position', 'absolute');
            this.renderer.setStyle(this.overlayTitle, 'width', '100%');
            this.renderer.setStyle(this.overlayTitle, 'height', '100%');
            this.renderer.setStyle(this.overlayTitle, 'text-align', 'center');
        }
        // color overlay
        this.renderer.setStyle(this.colorOverlay, 'background-color', this.originalToolbarBgColor);
        this.renderer.setStyle(this.colorOverlay, 'height', this.maximumHeight + "px");
        this.renderer.setStyle(this.colorOverlay, 'position', 'absolute');
        this.renderer.setStyle(this.colorOverlay, 'top', -this.headerMinHeight * 0 + "px");
        this.renderer.setStyle(this.colorOverlay, 'left', '0');
        this.renderer.setStyle(this.colorOverlay, 'width', '100%');
        this.renderer.setStyle(this.colorOverlay, 'z-index', '10');
        this.renderer.setStyle(this.colorOverlay, 'pointer-events', 'none');
        // image overlay
        this.renderer.setStyle(this.imageOverlay, 'background-color', this.expandedColor);
        this.renderer.setStyle(this.imageOverlay, 'background-image', "url(" + (this.imageUrl || '') + ")");
        this.renderer.setStyle(this.imageOverlay, 'height', "100%");
        this.renderer.setStyle(this.imageOverlay, 'width', '100%');
        this.renderer.setStyle(this.imageOverlay, 'pointer-events', 'none');
        this.renderer.setStyle(this.imageOverlay, 'background-size', 'cover');
        this.renderer.setStyle(this.imageOverlay, 'background-position', 'center');
        // .toolbar-background
        this.renderer.setStyle(this.toolbarBackground, 'background-color', this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            this.renderer.setStyle(this.barButtons, 'pointer-events', 'all');
            Array.from(this.barButtons.children).forEach((/**
             * @param {?} btn
             * @return {?}
             */
            function (btn) {
                _this.renderer.setStyle(btn, 'color', _this.titleColor);
            }));
        }
        // .scroll-content
        if (this.scrollContent) {
            this.renderer.setAttribute(this.scrollContent, 'parallax', '');
            this.renderer.setStyle(this.scrollContent, 'padding-top', this.maximumHeight + this.scrollContentPaddingTop - this.headerMinHeight + "px");
        }
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.initEvents = /**
     * @return {?}
     */
    function () {
        var _this = this;
        window.addEventListener('resize', (/**
         * @return {?}
         */
        function () {
            _this.headerHeight = _this.scrollContent.clientHeight;
        }), false);
        if (this.scrollContent) {
            this.scrollContent.addEventListener('scroll', (/**
             * @param {?} e
             * @return {?}
             */
            function (e) {
                if (!_this.ticking) {
                    window.requestAnimationFrame((/**
                     * @return {?}
                     */
                    function () {
                        _this.updateElasticHeader();
                    }));
                }
                _this.ticking = true;
            }));
        }
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.updateElasticHeader = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.scrollContent || !toolbar) {
            return;
        }
        this.scrollTop = this.scrollContent.scrollTop;
        if (this.scrollTop >= 0) {
            this.translateAmt = this.scrollTop / 2;
            this.scaleAmt = 1;
        }
        else {
            this.translateAmt = 0;
            this.scaleAmt = -this.scrollTop / this.headerHeight + 1;
        }
        // Parallax total progress
        this.headerMinHeight = this.toolbar.offsetHeight;
        /** @type {?} */
        var progress = (this.maximumHeight - this.scrollTop - this.headerMinHeight) / (this.maximumHeight - this.headerMinHeight);
        progress = Math.max(progress, 0);
        // ion-header: set height
        /** @type {?} */
        var targetHeight = this.maximumHeight - this.scrollTop;
        targetHeight = Math.max(targetHeight, this.headerMinHeight);
        // .toolbar-background: change color
        this.renderer.setStyle(this.imageOverlay, 'height', targetHeight + "px");
        this.renderer.setStyle(this.imageOverlay, 'opacity', "" + progress);
        this.renderer.setStyle(this.colorOverlay, 'height', targetHeight + "px");
        this.renderer.setStyle(this.colorOverlay, 'opacity', targetHeight > this.headerMinHeight ? '1' : '0');
        this.renderer.setStyle(this.toolbarBackground, 'background-color', targetHeight > this.headerMinHeight ? 'transparent' : this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            if (targetHeight > this.headerMinHeight) {
                this.imageOverlay.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                function (btn) {
                    _this.renderer.setStyle(btn, 'color', _this.titleColor);
                }));
            }
            else {
                this.toolbar.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                function (btn) {
                    _this.renderer.setStyle(btn, 'color', 'unset');
                }));
            }
        }
        this.ticking = false;
    };
    ParallaxDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'ion-header[parallax]'
                },] }
    ];
    /** @nocollapse */
    ParallaxDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    ParallaxDirective.propDecorators = {
        imageUrl: [{ type: Input }],
        expandedColor: [{ type: Input }],
        titleColor: [{ type: Input }],
        maximumHeight: [{ type: Input }]
    };
    return ParallaxDirective;
}());
if (false) {
    /** @type {?} */
    ParallaxDirective.prototype.imageUrl;
    /** @type {?} */
    ParallaxDirective.prototype.expandedColor;
    /** @type {?} */
    ParallaxDirective.prototype.titleColor;
    /** @type {?} */
    ParallaxDirective.prototype.maximumHeight;
    /** @type {?} */
    ParallaxDirective.prototype.header;
    /** @type {?} */
    ParallaxDirective.prototype.toolbar;
    /** @type {?} */
    ParallaxDirective.prototype.toolbarBackground;
    /** @type {?} */
    ParallaxDirective.prototype.imageOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.colorOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.barButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContent;
    /** @type {?} */
    ParallaxDirective.prototype.headerHeight;
    /** @type {?} */
    ParallaxDirective.prototype.headerMinHeight;
    /** @type {?} */
    ParallaxDirective.prototype.translateAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scaleAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.lastScrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.ticking;
    /** @type {?} */
    ParallaxDirective.prototype.originalToolbarBgColor;
    /** @type {?} */
    ParallaxDirective.prototype.overlayTitle;
    /** @type {?} */
    ParallaxDirective.prototype.ionTitle;
    /** @type {?} */
    ParallaxDirective.prototype.overlayButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContentPaddingTop;
    /** @type {?} */
    ParallaxDirective.prototype.headerRef;
    /** @type {?} */
    ParallaxDirective.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var IonicHeaderParallaxModule = /** @class */ (function () {
    function IonicHeaderParallaxModule() {
    }
    IonicHeaderParallaxModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        ParallaxDirective
                    ],
                    imports: [],
                    exports: [
                        ParallaxDirective
                    ]
                },] }
    ];
    return IonicHeaderParallaxModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { IonicHeaderParallaxModule, ParallaxDirective };
//# sourceMappingURL=ionic-header-parallax.js.map
