/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
var ParallaxDirective = /** @class */ (function () {
    function ParallaxDirective(headerRef, renderer) {
        this.headerRef = headerRef;
        this.renderer = renderer;
        this.maximumHeight = 300;
    }
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () {
            _this.initElements();
            _this.initStyles();
            _this.initEvents();
        }), 100);
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.initElements = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var parentElement = this.headerRef.nativeElement.parentElement;
        this.header = this.headerRef.nativeElement;
        this.toolbar = this.header.querySelector('ion-toolbar');
        if (!this.toolbar) {
            throw new Error('Parallax directive requires a toolbar or navbar element on the page to work.');
        }
        this.ionTitle = this.toolbar.querySelector('ion-title');
        this.toolbarBackground = this.toolbar.shadowRoot.querySelector('.toolbar-background');
        this.barButtons = this.headerRef.nativeElement.querySelector('ion-buttons');
        /** @type {?} */
        var ionContent = parentElement.querySelector('ion-content');
        this.scrollContent = ionContent.shadowRoot.querySelector('.inner-scroll');
        if (!this.scrollContent) {
            throw new Error('Parallax directive requires an <ion-content> element on the page to work.');
        }
        // Create image overlay
        this.imageOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.imageOverlay, 'image-overlay');
        this.colorOverlay = this.renderer.createElement('div');
        this.renderer.addClass(this.colorOverlay, 'color-overlay');
        this.colorOverlay.appendChild(this.imageOverlay);
        this.header.appendChild(this.colorOverlay);
        // Copy title and buttons
        this.overlayTitle = this.ionTitle && (/** @type {?} */ (this.ionTitle.cloneNode(true)));
        if (this.overlayTitle) {
            this.renderer.addClass(this.overlayTitle, 'parallax-title');
            setTimeout((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var toolbarTitle = _this.overlayTitle.shadowRoot.querySelector('.toolbar-title');
                _this.renderer.setStyle(toolbarTitle, 'pointer-events', 'unset');
            }));
        }
        if (this.overlayTitle) {
            this.imageOverlay.appendChild(this.overlayTitle);
        }
        if (this.barButtons) {
            this.imageOverlay.appendChild(this.barButtons);
        }
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.initStyles = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.headerHeight = this.scrollContent.clientHeight;
        this.ticking = false;
        if (!this.scrollContent || !toolbar) {
            return;
        }
        // fetch styles
        this.maximumHeight = parseFloat(this.maximumHeight.toString());
        this.headerMinHeight = this.toolbar.offsetHeight;
        this.scrollContentPaddingTop = window.getComputedStyle(this.scrollContent, null).paddingTop.replace('px', '');
        this.scrollContentPaddingTop = parseFloat(this.scrollContentPaddingTop);
        this.originalToolbarBgColor = window.getComputedStyle(this.toolbarBackground, null).backgroundColor;
        // header and title
        this.renderer.setStyle(this.header, 'position', 'relative');
        if (this.overlayTitle) {
            this.renderer.setStyle(this.overlayTitle, 'color', this.titleColor);
            this.renderer.setStyle(this.overlayTitle, 'position', 'absolute');
            this.renderer.setStyle(this.overlayTitle, 'width', '100%');
            this.renderer.setStyle(this.overlayTitle, 'height', '100%');
            this.renderer.setStyle(this.overlayTitle, 'text-align', 'center');
        }
        // color overlay
        this.renderer.setStyle(this.colorOverlay, 'background-color', this.originalToolbarBgColor);
        this.renderer.setStyle(this.colorOverlay, 'height', this.maximumHeight + "px");
        this.renderer.setStyle(this.colorOverlay, 'position', 'absolute');
        this.renderer.setStyle(this.colorOverlay, 'top', -this.headerMinHeight * 0 + "px");
        this.renderer.setStyle(this.colorOverlay, 'left', '0');
        this.renderer.setStyle(this.colorOverlay, 'width', '100%');
        this.renderer.setStyle(this.colorOverlay, 'z-index', '10');
        this.renderer.setStyle(this.colorOverlay, 'pointer-events', 'none');
        // image overlay
        this.renderer.setStyle(this.imageOverlay, 'background-color', this.expandedColor);
        this.renderer.setStyle(this.imageOverlay, 'background-image', "url(" + (this.imageUrl || '') + ")");
        this.renderer.setStyle(this.imageOverlay, 'height', "100%");
        this.renderer.setStyle(this.imageOverlay, 'width', '100%');
        this.renderer.setStyle(this.imageOverlay, 'pointer-events', 'none');
        this.renderer.setStyle(this.imageOverlay, 'background-size', 'cover');
        this.renderer.setStyle(this.imageOverlay, 'background-position', 'center');
        // .toolbar-background
        this.renderer.setStyle(this.toolbarBackground, 'background-color', this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            this.renderer.setStyle(this.barButtons, 'pointer-events', 'all');
            Array.from(this.barButtons.children).forEach((/**
             * @param {?} btn
             * @return {?}
             */
            function (btn) {
                _this.renderer.setStyle(btn, 'color', _this.titleColor);
            }));
        }
        // .scroll-content
        if (this.scrollContent) {
            this.renderer.setAttribute(this.scrollContent, 'parallax', '');
            this.renderer.setStyle(this.scrollContent, 'padding-top', this.maximumHeight + this.scrollContentPaddingTop - this.headerMinHeight + "px");
        }
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.initEvents = /**
     * @return {?}
     */
    function () {
        var _this = this;
        window.addEventListener('resize', (/**
         * @return {?}
         */
        function () {
            _this.headerHeight = _this.scrollContent.clientHeight;
        }), false);
        if (this.scrollContent) {
            this.scrollContent.addEventListener('scroll', (/**
             * @param {?} e
             * @return {?}
             */
            function (e) {
                if (!_this.ticking) {
                    window.requestAnimationFrame((/**
                     * @return {?}
                     */
                    function () {
                        _this.updateElasticHeader();
                    }));
                }
                _this.ticking = true;
            }));
        }
    };
    /**
     * @return {?}
     */
    ParallaxDirective.prototype.updateElasticHeader = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.scrollContent || !toolbar) {
            return;
        }
        this.scrollTop = this.scrollContent.scrollTop;
        if (this.scrollTop >= 0) {
            this.translateAmt = this.scrollTop / 2;
            this.scaleAmt = 1;
        }
        else {
            this.translateAmt = 0;
            this.scaleAmt = -this.scrollTop / this.headerHeight + 1;
        }
        // Parallax total progress
        this.headerMinHeight = this.toolbar.offsetHeight;
        /** @type {?} */
        var progress = (this.maximumHeight - this.scrollTop - this.headerMinHeight) / (this.maximumHeight - this.headerMinHeight);
        progress = Math.max(progress, 0);
        // ion-header: set height
        /** @type {?} */
        var targetHeight = this.maximumHeight - this.scrollTop;
        targetHeight = Math.max(targetHeight, this.headerMinHeight);
        // .toolbar-background: change color
        this.renderer.setStyle(this.imageOverlay, 'height', targetHeight + "px");
        this.renderer.setStyle(this.imageOverlay, 'opacity', "" + progress);
        this.renderer.setStyle(this.colorOverlay, 'height', targetHeight + "px");
        this.renderer.setStyle(this.colorOverlay, 'opacity', targetHeight > this.headerMinHeight ? '1' : '0');
        this.renderer.setStyle(this.toolbarBackground, 'background-color', targetHeight > this.headerMinHeight ? 'transparent' : this.originalToolbarBgColor);
        // .bar-buttons
        if (this.barButtons) {
            if (targetHeight > this.headerMinHeight) {
                this.imageOverlay.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                function (btn) {
                    _this.renderer.setStyle(btn, 'color', _this.titleColor);
                }));
            }
            else {
                this.toolbar.append(this.barButtons);
                Array.from(this.barButtons.children).forEach((/**
                 * @param {?} btn
                 * @return {?}
                 */
                function (btn) {
                    _this.renderer.setStyle(btn, 'color', 'unset');
                }));
            }
        }
        this.ticking = false;
    };
    ParallaxDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'ion-header[parallax]'
                },] }
    ];
    /** @nocollapse */
    ParallaxDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    ParallaxDirective.propDecorators = {
        imageUrl: [{ type: Input }],
        expandedColor: [{ type: Input }],
        titleColor: [{ type: Input }],
        maximumHeight: [{ type: Input }]
    };
    return ParallaxDirective;
}());
export { ParallaxDirective };
if (false) {
    /** @type {?} */
    ParallaxDirective.prototype.imageUrl;
    /** @type {?} */
    ParallaxDirective.prototype.expandedColor;
    /** @type {?} */
    ParallaxDirective.prototype.titleColor;
    /** @type {?} */
    ParallaxDirective.prototype.maximumHeight;
    /** @type {?} */
    ParallaxDirective.prototype.header;
    /** @type {?} */
    ParallaxDirective.prototype.toolbar;
    /** @type {?} */
    ParallaxDirective.prototype.toolbarBackground;
    /** @type {?} */
    ParallaxDirective.prototype.imageOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.colorOverlay;
    /** @type {?} */
    ParallaxDirective.prototype.barButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContent;
    /** @type {?} */
    ParallaxDirective.prototype.headerHeight;
    /** @type {?} */
    ParallaxDirective.prototype.headerMinHeight;
    /** @type {?} */
    ParallaxDirective.prototype.translateAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scaleAmt;
    /** @type {?} */
    ParallaxDirective.prototype.scrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.lastScrollTop;
    /** @type {?} */
    ParallaxDirective.prototype.ticking;
    /** @type {?} */
    ParallaxDirective.prototype.originalToolbarBgColor;
    /** @type {?} */
    ParallaxDirective.prototype.overlayTitle;
    /** @type {?} */
    ParallaxDirective.prototype.ionTitle;
    /** @type {?} */
    ParallaxDirective.prototype.overlayButtons;
    /** @type {?} */
    ParallaxDirective.prototype.scrollContentPaddingTop;
    /** @type {?} */
    ParallaxDirective.prototype.headerRef;
    /** @type {?} */
    ParallaxDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFyYWxsYXguZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaW9uaWMtaGVhZGVyLXBhcmFsbGF4LyIsInNvdXJjZXMiOlsibGliL3BhcmFsbGF4LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFdkY7SUE2QkUsMkJBQW1CLFNBQWtDLEVBQVMsUUFBbUI7UUFBOUQsY0FBUyxHQUFULFNBQVMsQ0FBeUI7UUFBUyxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBdEJ4RSxrQkFBYSxHQUFHLEdBQUcsQ0FBQztJQXVCN0IsQ0FBQzs7OztJQUVELDJDQUFlOzs7SUFBZjtRQUFBLGlCQU1DO1FBTEMsVUFBVTs7O1FBQUM7WUFDVCxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2xCLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNwQixDQUFDLEdBQUUsR0FBRyxDQUFDLENBQUM7SUFDVixDQUFDOzs7O0lBRUQsd0NBQVk7OztJQUFaO1FBQUEsaUJBb0NDOztZQW5DTyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsYUFBYTtRQUNoRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDO1FBRTNDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLDhFQUE4RSxDQUFDLENBQUM7U0FBRTtRQUN2SCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUV0RixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7WUFDdEUsVUFBVSxHQUFHLGFBQWEsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDO1FBQzdELElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLDJFQUEyRSxDQUFDLENBQUM7U0FBRTtRQUUxSCx1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBRTNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxlQUFlLENBQUMsQ0FBQztRQUUzRCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRTNDLHlCQUF5QjtRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLElBQUksbUJBQUEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQWUsQ0FBQztRQUNsRixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzVELFVBQVU7OztZQUFDOztvQkFDSCxZQUFZLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDO2dCQUNqRixLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDbEUsQ0FBQyxFQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUFFO1FBQzVFLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUFFO0lBQzFFLENBQUM7Ozs7SUFFRCxzQ0FBVTs7O0lBQVY7UUFBQSxpQkEyREM7UUExREMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztRQUNwRCxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUVyQixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUFFLE9BQU87U0FBRTtRQUVoRCxlQUFlO1FBQ2YsSUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7UUFDakQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzlHLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLHNCQUFzQixHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUMsZUFBZSxDQUFDO1FBRXBHLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUM1RCxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQ2xFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsWUFBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ25FO1FBRUQsZ0JBQWdCO1FBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDM0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUssSUFBSSxDQUFDLGFBQWEsT0FBSSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsT0FBSSxDQUFDLENBQUM7UUFDbkYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUVwRSxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxrQkFBa0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxrQkFBa0IsRUFBRSxVQUFPLElBQUksQ0FBQyxRQUFRLElBQUksRUFBRSxPQUFHLENBQUMsQ0FBQztRQUM3RixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsaUJBQWlCLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxxQkFBcUIsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUUzRSxzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLGtCQUFrQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBRWhHLGVBQWU7UUFDZixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNqRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTzs7OztZQUFDLFVBQUEsR0FBRztnQkFDOUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDeEQsQ0FBQyxFQUFDLENBQUM7U0FDSjtRQUVELGtCQUFrQjtRQUNsQixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxhQUFhLEVBQ25ELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxlQUFlLE9BQUksQ0FBQyxDQUFDO1NBQ3BGO0lBQ0gsQ0FBQzs7OztJQUVELHNDQUFVOzs7SUFBVjtRQUFBLGlCQWdCQztRQWZDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFROzs7UUFBRTtZQUNoQyxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1FBQ3RELENBQUMsR0FBRSxLQUFLLENBQUMsQ0FBQztRQUVWLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLFFBQVE7Ozs7WUFBRSxVQUFDLENBQUM7Z0JBRTlDLElBQUksQ0FBQyxLQUFJLENBQUMsT0FBTyxFQUFFO29CQUNqQixNQUFNLENBQUMscUJBQXFCOzs7b0JBQUM7d0JBQzNCLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO29CQUM3QixDQUFDLEVBQUMsQ0FBQztpQkFDSjtnQkFDRCxLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUN0QixDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7OztJQUVELCtDQUFtQjs7O0lBQW5CO1FBQUEsaUJBNkNDO1FBNUNDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQUUsT0FBTztTQUFFO1FBRWhELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7UUFDOUMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1NBQ25CO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztTQUN6RDtRQUVELDBCQUEwQjtRQUMxQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDOztZQUM3QyxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQ3pILFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQzs7O1lBRzdCLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTO1FBQ3RELFlBQVksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFFNUQsb0NBQW9DO1FBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxFQUFLLFlBQVksT0FBSSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsS0FBRyxRQUFVLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBSyxZQUFZLE9BQUksQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsU0FBUyxFQUFFLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxrQkFBa0IsRUFDL0QsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFFckYsZUFBZTtRQUNmLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN2QyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUEsR0FBRztvQkFDOUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hELENBQUMsRUFBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNyQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTzs7OztnQkFBQyxVQUFBLEdBQUc7b0JBQzlDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ2hELENBQUMsRUFBQyxDQUFDO2FBQ0o7U0FDRjtRQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7O2dCQTFNRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtpQkFDakM7Ozs7Z0JBSm1CLFVBQVU7Z0JBQVMsU0FBUzs7OzJCQU03QyxLQUFLO2dDQUNMLEtBQUs7NkJBQ0wsS0FBSztnQ0FDTCxLQUFLOztJQW9NUix3QkFBQztDQUFBLEFBM01ELElBMk1DO1NBeE1ZLGlCQUFpQjs7O0lBQzVCLHFDQUEwQjs7SUFDMUIsMENBQStCOztJQUMvQix1Q0FBNEI7O0lBQzVCLDBDQUE2Qjs7SUFFN0IsbUNBQW9COztJQUNwQixvQ0FBcUI7O0lBQ3JCLDhDQUErQjs7SUFDL0IseUNBQTBCOztJQUMxQix5Q0FBMEI7O0lBQzFCLHVDQUF3Qjs7SUFDeEIsMENBQTJCOztJQUMzQix5Q0FBa0I7O0lBQ2xCLDRDQUF3Qjs7SUFDeEIseUNBQWtCOztJQUNsQixxQ0FBYzs7SUFDZCxzQ0FBZTs7SUFDZiwwQ0FBbUI7O0lBQ25CLG9DQUFhOztJQUNiLG1EQUErQjs7SUFDL0IseUNBQTBCOztJQUMxQixxQ0FBc0I7O0lBQ3RCLDJDQUE4Qjs7SUFDOUIsb0RBQXdCOztJQUVaLHNDQUF5Qzs7SUFBRSxxQ0FBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIElucHV0LCBSZW5kZXJlcjIsIEFmdGVyVmlld0luaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiAnaW9uLWhlYWRlcltwYXJhbGxheF0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYXJhbGxheERpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xyXG4gIEBJbnB1dCgpIGltYWdlVXJsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgZXhwYW5kZWRDb2xvcjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHRpdGxlQ29sb3I6IHN0cmluZztcclxuICBASW5wdXQoKSBtYXhpbXVtSGVpZ2h0ID0gMzAwO1xyXG5cclxuICBoZWFkZXI6IEhUTUxFbGVtZW50O1xyXG4gIHRvb2xiYXI6IEhUTUxFbGVtZW50O1xyXG4gIHRvb2xiYXJCYWNrZ3JvdW5kOiBIVE1MRWxlbWVudDtcclxuICBpbWFnZU92ZXJsYXk6IEhUTUxFbGVtZW50O1xyXG4gIGNvbG9yT3ZlcmxheTogSFRNTEVsZW1lbnQ7XHJcbiAgYmFyQnV0dG9uczogSFRNTEVsZW1lbnQ7XHJcbiAgc2Nyb2xsQ29udGVudDogSFRNTEVsZW1lbnQ7XHJcbiAgaGVhZGVySGVpZ2h0OiBhbnk7XHJcbiAgaGVhZGVyTWluSGVpZ2h0OiBudW1iZXI7XHJcbiAgdHJhbnNsYXRlQW10OiBhbnk7XHJcbiAgc2NhbGVBbXQ6IGFueTtcclxuICBzY3JvbGxUb3A6IGFueTtcclxuICBsYXN0U2Nyb2xsVG9wOiBhbnk7XHJcbiAgdGlja2luZzogYW55O1xyXG4gIG9yaWdpbmFsVG9vbGJhckJnQ29sb3I6IHN0cmluZztcclxuICBvdmVybGF5VGl0bGU6IEhUTUxFbGVtZW50O1xyXG4gIGlvblRpdGxlOiBIVE1MRWxlbWVudDtcclxuICBvdmVybGF5QnV0dG9uczogSFRNTEVsZW1lbnRbXTtcclxuICBzY3JvbGxDb250ZW50UGFkZGluZ1RvcDtcclxuXHJcbiAgY29uc3RydWN0b3IocHVibGljIGhlYWRlclJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sIHB1YmxpYyByZW5kZXJlcjogUmVuZGVyZXIyKSB7XHJcbiAgfVxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgdGhpcy5pbml0RWxlbWVudHMoKTtcclxuICAgICAgdGhpcy5pbml0U3R5bGVzKCk7XHJcbiAgICAgIHRoaXMuaW5pdEV2ZW50cygpO1xyXG4gICAgfSwgMTAwKTtcclxuICB9XHJcblxyXG4gIGluaXRFbGVtZW50cygpIHtcclxuICAgIGNvbnN0IHBhcmVudEVsZW1lbnQgPSB0aGlzLmhlYWRlclJlZi5uYXRpdmVFbGVtZW50LnBhcmVudEVsZW1lbnQ7XHJcbiAgICB0aGlzLmhlYWRlciA9IHRoaXMuaGVhZGVyUmVmLm5hdGl2ZUVsZW1lbnQ7XHJcblxyXG4gICAgdGhpcy50b29sYmFyID0gdGhpcy5oZWFkZXIucXVlcnlTZWxlY3RvcignaW9uLXRvb2xiYXInKTtcclxuICAgIGlmICghdGhpcy50b29sYmFyKSB7IHRocm93IG5ldyBFcnJvcignUGFyYWxsYXggZGlyZWN0aXZlIHJlcXVpcmVzIGEgdG9vbGJhciBvciBuYXZiYXIgZWxlbWVudCBvbiB0aGUgcGFnZSB0byB3b3JrLicpOyB9XHJcbiAgICB0aGlzLmlvblRpdGxlID0gdGhpcy50b29sYmFyLnF1ZXJ5U2VsZWN0b3IoJ2lvbi10aXRsZScpO1xyXG4gICAgdGhpcy50b29sYmFyQmFja2dyb3VuZCA9IHRoaXMudG9vbGJhci5zaGFkb3dSb290LnF1ZXJ5U2VsZWN0b3IoJy50b29sYmFyLWJhY2tncm91bmQnKTtcclxuXHJcbiAgICB0aGlzLmJhckJ1dHRvbnMgPSB0aGlzLmhlYWRlclJlZi5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2lvbi1idXR0b25zJyk7XHJcbiAgICBjb25zdCBpb25Db250ZW50ID0gcGFyZW50RWxlbWVudC5xdWVyeVNlbGVjdG9yKCdpb24tY29udGVudCcpO1xyXG4gICAgdGhpcy5zY3JvbGxDb250ZW50ID0gaW9uQ29udGVudC5zaGFkb3dSb290LnF1ZXJ5U2VsZWN0b3IoJy5pbm5lci1zY3JvbGwnKTtcclxuICAgIGlmICghdGhpcy5zY3JvbGxDb250ZW50KSB7IHRocm93IG5ldyBFcnJvcignUGFyYWxsYXggZGlyZWN0aXZlIHJlcXVpcmVzIGFuIDxpb24tY29udGVudD4gZWxlbWVudCBvbiB0aGUgcGFnZSB0byB3b3JrLicpOyB9XHJcblxyXG4gICAgLy8gQ3JlYXRlIGltYWdlIG92ZXJsYXlcclxuICAgIHRoaXMuaW1hZ2VPdmVybGF5ID0gdGhpcy5yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5pbWFnZU92ZXJsYXksICdpbWFnZS1vdmVybGF5Jyk7XHJcblxyXG4gICAgdGhpcy5jb2xvck92ZXJsYXkgPSB0aGlzLnJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLmNvbG9yT3ZlcmxheSwgJ2NvbG9yLW92ZXJsYXknKTtcclxuXHJcbiAgICB0aGlzLmNvbG9yT3ZlcmxheS5hcHBlbmRDaGlsZCh0aGlzLmltYWdlT3ZlcmxheSk7XHJcbiAgICB0aGlzLmhlYWRlci5hcHBlbmRDaGlsZCh0aGlzLmNvbG9yT3ZlcmxheSk7XHJcblxyXG4gICAgLy8gQ29weSB0aXRsZSBhbmQgYnV0dG9uc1xyXG4gICAgdGhpcy5vdmVybGF5VGl0bGUgPSB0aGlzLmlvblRpdGxlICYmIHRoaXMuaW9uVGl0bGUuY2xvbmVOb2RlKHRydWUpIGFzIEhUTUxFbGVtZW50O1xyXG4gICAgaWYgKHRoaXMub3ZlcmxheVRpdGxlKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5vdmVybGF5VGl0bGUsICdwYXJhbGxheC10aXRsZScpO1xyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICBjb25zdCB0b29sYmFyVGl0bGUgPSB0aGlzLm92ZXJsYXlUaXRsZS5zaGFkb3dSb290LnF1ZXJ5U2VsZWN0b3IoJy50b29sYmFyLXRpdGxlJyk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0b29sYmFyVGl0bGUsICdwb2ludGVyLWV2ZW50cycsICd1bnNldCcpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5vdmVybGF5VGl0bGUpIHsgdGhpcy5pbWFnZU92ZXJsYXkuYXBwZW5kQ2hpbGQodGhpcy5vdmVybGF5VGl0bGUpOyB9XHJcbiAgICBpZiAodGhpcy5iYXJCdXR0b25zKSB7IHRoaXMuaW1hZ2VPdmVybGF5LmFwcGVuZENoaWxkKHRoaXMuYmFyQnV0dG9ucyk7IH1cclxuICB9XHJcblxyXG4gIGluaXRTdHlsZXMoKSB7XHJcbiAgICB0aGlzLmhlYWRlckhlaWdodCA9IHRoaXMuc2Nyb2xsQ29udGVudC5jbGllbnRIZWlnaHQ7XHJcbiAgICB0aGlzLnRpY2tpbmcgPSBmYWxzZTtcclxuXHJcbiAgICBpZiAoIXRoaXMuc2Nyb2xsQ29udGVudCB8fCAhdG9vbGJhcikgeyByZXR1cm47IH1cclxuXHJcbiAgICAvLyBmZXRjaCBzdHlsZXNcclxuICAgIHRoaXMubWF4aW11bUhlaWdodCA9IHBhcnNlRmxvYXQodGhpcy5tYXhpbXVtSGVpZ2h0LnRvU3RyaW5nKCkpO1xyXG4gICAgdGhpcy5oZWFkZXJNaW5IZWlnaHQgPSB0aGlzLnRvb2xiYXIub2Zmc2V0SGVpZ2h0O1xyXG4gICAgdGhpcy5zY3JvbGxDb250ZW50UGFkZGluZ1RvcCA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRoaXMuc2Nyb2xsQ29udGVudCwgbnVsbCkucGFkZGluZ1RvcC5yZXBsYWNlKCdweCcsICcnKTtcclxuICAgIHRoaXMuc2Nyb2xsQ29udGVudFBhZGRpbmdUb3AgPSBwYXJzZUZsb2F0KHRoaXMuc2Nyb2xsQ29udGVudFBhZGRpbmdUb3ApO1xyXG4gICAgdGhpcy5vcmlnaW5hbFRvb2xiYXJCZ0NvbG9yID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUodGhpcy50b29sYmFyQmFja2dyb3VuZCwgbnVsbCkuYmFja2dyb3VuZENvbG9yO1xyXG5cclxuICAgIC8vIGhlYWRlciBhbmQgdGl0bGVcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5oZWFkZXIsICdwb3NpdGlvbicsICdyZWxhdGl2ZScpO1xyXG4gICAgaWYgKHRoaXMub3ZlcmxheVRpdGxlKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5vdmVybGF5VGl0bGUsICdjb2xvcicsIHRoaXMudGl0bGVDb2xvcik7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5vdmVybGF5VGl0bGUsICdwb3NpdGlvbicsICdhYnNvbHV0ZScpO1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMub3ZlcmxheVRpdGxlLCAnd2lkdGgnLCAnMTAwJScpO1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMub3ZlcmxheVRpdGxlLCAnaGVpZ2h0JywgJzEwMCUnKTtcclxuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLm92ZXJsYXlUaXRsZSwgJ3RleHQtYWxpZ24nLCAnY2VudGVyJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gY29sb3Igb3ZlcmxheVxyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ2JhY2tncm91bmQtY29sb3InLCB0aGlzLm9yaWdpbmFsVG9vbGJhckJnQ29sb3IpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ2hlaWdodCcsIGAke3RoaXMubWF4aW11bUhlaWdodH1weGApO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ3Bvc2l0aW9uJywgJ2Fic29sdXRlJyk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY29sb3JPdmVybGF5LCAndG9wJywgYCR7LXRoaXMuaGVhZGVyTWluSGVpZ2h0ICogMH1weGApO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ2xlZnQnLCAnMCcpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNvbG9yT3ZlcmxheSwgJ3dpZHRoJywgJzEwMCUnKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5jb2xvck92ZXJsYXksICd6LWluZGV4JywgJzEwJyk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY29sb3JPdmVybGF5LCAncG9pbnRlci1ldmVudHMnLCAnbm9uZScpO1xyXG5cclxuICAgIC8vIGltYWdlIG92ZXJsYXlcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5pbWFnZU92ZXJsYXksICdiYWNrZ3JvdW5kLWNvbG9yJywgdGhpcy5leHBhbmRlZENvbG9yKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5pbWFnZU92ZXJsYXksICdiYWNrZ3JvdW5kLWltYWdlJywgYHVybCgke3RoaXMuaW1hZ2VVcmwgfHwgJyd9KWApO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ2hlaWdodCcsIGAxMDAlYCk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuaW1hZ2VPdmVybGF5LCAnd2lkdGgnLCAnMTAwJScpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ3BvaW50ZXItZXZlbnRzJywgJ25vbmUnKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5pbWFnZU92ZXJsYXksICdiYWNrZ3JvdW5kLXNpemUnLCAnY292ZXInKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5pbWFnZU92ZXJsYXksICdiYWNrZ3JvdW5kLXBvc2l0aW9uJywgJ2NlbnRlcicpO1xyXG5cclxuICAgIC8vIC50b29sYmFyLWJhY2tncm91bmRcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy50b29sYmFyQmFja2dyb3VuZCwgJ2JhY2tncm91bmQtY29sb3InLCB0aGlzLm9yaWdpbmFsVG9vbGJhckJnQ29sb3IpO1xyXG5cclxuICAgIC8vIC5iYXItYnV0dG9uc1xyXG4gICAgaWYgKHRoaXMuYmFyQnV0dG9ucykge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuYmFyQnV0dG9ucywgJ3BvaW50ZXItZXZlbnRzJywgJ2FsbCcpO1xyXG4gICAgICBBcnJheS5mcm9tKHRoaXMuYmFyQnV0dG9ucy5jaGlsZHJlbikuZm9yRWFjaChidG4gPT4ge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoYnRuLCAnY29sb3InLCB0aGlzLnRpdGxlQ29sb3IpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAuc2Nyb2xsLWNvbnRlbnRcclxuICAgIGlmICh0aGlzLnNjcm9sbENvbnRlbnQpIHtcclxuICAgICAgdGhpcy5yZW5kZXJlci5zZXRBdHRyaWJ1dGUodGhpcy5zY3JvbGxDb250ZW50LCAncGFyYWxsYXgnLCAnJyk7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5zY3JvbGxDb250ZW50LCAncGFkZGluZy10b3AnLFxyXG4gICAgICAgIGAke3RoaXMubWF4aW11bUhlaWdodCArIHRoaXMuc2Nyb2xsQ29udGVudFBhZGRpbmdUb3AgLSB0aGlzLmhlYWRlck1pbkhlaWdodH1weGApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaW5pdEV2ZW50cygpIHtcclxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCAoKSA9PiB7XHJcbiAgICAgIHRoaXMuaGVhZGVySGVpZ2h0ID0gdGhpcy5zY3JvbGxDb250ZW50LmNsaWVudEhlaWdodDtcclxuICAgIH0sIGZhbHNlKTtcclxuXHJcbiAgICBpZiAodGhpcy5zY3JvbGxDb250ZW50KSB7XHJcbiAgICAgIHRoaXMuc2Nyb2xsQ29udGVudC5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCAoZSkgPT4ge1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMudGlja2luZykge1xyXG4gICAgICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlRWxhc3RpY0hlYWRlcigpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudGlja2luZyA9IHRydWU7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdXBkYXRlRWxhc3RpY0hlYWRlcigpIHtcclxuICAgIGlmICghdGhpcy5zY3JvbGxDb250ZW50IHx8ICF0b29sYmFyKSB7IHJldHVybjsgfVxyXG5cclxuICAgIHRoaXMuc2Nyb2xsVG9wID0gdGhpcy5zY3JvbGxDb250ZW50LnNjcm9sbFRvcDtcclxuICAgIGlmICh0aGlzLnNjcm9sbFRvcCA+PSAwKSB7XHJcbiAgICAgIHRoaXMudHJhbnNsYXRlQW10ID0gdGhpcy5zY3JvbGxUb3AgLyAyO1xyXG4gICAgICB0aGlzLnNjYWxlQW10ID0gMTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudHJhbnNsYXRlQW10ID0gMDtcclxuICAgICAgdGhpcy5zY2FsZUFtdCA9IC10aGlzLnNjcm9sbFRvcCAvIHRoaXMuaGVhZGVySGVpZ2h0ICsgMTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBQYXJhbGxheCB0b3RhbCBwcm9ncmVzc1xyXG4gICAgdGhpcy5oZWFkZXJNaW5IZWlnaHQgPSB0aGlzLnRvb2xiYXIub2Zmc2V0SGVpZ2h0O1xyXG4gICAgbGV0IHByb2dyZXNzID0gKHRoaXMubWF4aW11bUhlaWdodCAtIHRoaXMuc2Nyb2xsVG9wIC0gdGhpcy5oZWFkZXJNaW5IZWlnaHQpIC8gKHRoaXMubWF4aW11bUhlaWdodCAtIHRoaXMuaGVhZGVyTWluSGVpZ2h0KTtcclxuICAgIHByb2dyZXNzID0gTWF0aC5tYXgocHJvZ3Jlc3MsIDApO1xyXG5cclxuICAgIC8vIGlvbi1oZWFkZXI6IHNldCBoZWlnaHRcclxuICAgIGxldCB0YXJnZXRIZWlnaHQgPSB0aGlzLm1heGltdW1IZWlnaHQgLSB0aGlzLnNjcm9sbFRvcDtcclxuICAgIHRhcmdldEhlaWdodCA9IE1hdGgubWF4KHRhcmdldEhlaWdodCwgdGhpcy5oZWFkZXJNaW5IZWlnaHQpO1xyXG5cclxuICAgIC8vIC50b29sYmFyLWJhY2tncm91bmQ6IGNoYW5nZSBjb2xvclxyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ2hlaWdodCcsIGAke3RhcmdldEhlaWdodH1weGApO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmltYWdlT3ZlcmxheSwgJ29wYWNpdHknLCBgJHtwcm9ncmVzc31gKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5jb2xvck92ZXJsYXksICdoZWlnaHQnLCBgJHt0YXJnZXRIZWlnaHR9cHhgKTtcclxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5jb2xvck92ZXJsYXksICdvcGFjaXR5JywgdGFyZ2V0SGVpZ2h0ID4gdGhpcy5oZWFkZXJNaW5IZWlnaHQgPyAnMScgOiAnMCcpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLnRvb2xiYXJCYWNrZ3JvdW5kLCAnYmFja2dyb3VuZC1jb2xvcicsXHJcbiAgICAgIHRhcmdldEhlaWdodCA+IHRoaXMuaGVhZGVyTWluSGVpZ2h0ID8gJ3RyYW5zcGFyZW50JyA6IHRoaXMub3JpZ2luYWxUb29sYmFyQmdDb2xvcik7XHJcblxyXG4gICAgLy8gLmJhci1idXR0b25zXHJcbiAgICBpZiAodGhpcy5iYXJCdXR0b25zKSB7XHJcbiAgICAgIGlmICh0YXJnZXRIZWlnaHQgPiB0aGlzLmhlYWRlck1pbkhlaWdodCkge1xyXG4gICAgICAgIHRoaXMuaW1hZ2VPdmVybGF5LmFwcGVuZCh0aGlzLmJhckJ1dHRvbnMpO1xyXG4gICAgICAgIEFycmF5LmZyb20odGhpcy5iYXJCdXR0b25zLmNoaWxkcmVuKS5mb3JFYWNoKGJ0biA9PiB7XHJcbiAgICAgICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGJ0biwgJ2NvbG9yJywgdGhpcy50aXRsZUNvbG9yKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnRvb2xiYXIuYXBwZW5kKHRoaXMuYmFyQnV0dG9ucyk7XHJcbiAgICAgICAgQXJyYXkuZnJvbSh0aGlzLmJhckJ1dHRvbnMuY2hpbGRyZW4pLmZvckVhY2goYnRuID0+IHtcclxuICAgICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoYnRuLCAnY29sb3InLCAndW5zZXQnKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMudGlja2luZyA9IGZhbHNlO1xyXG4gIH1cclxufVxyXG4iXX0=